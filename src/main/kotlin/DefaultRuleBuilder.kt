interface DefaultRuleBuilder{
    fun build(position: Int): String
}

class StayInSpaceBuilder : DefaultRuleBuilder {
    override fun build(position: Int) = "Stay in space $position"
}