import java.lang.IllegalArgumentException

class GameBoard(
        private val rules: List<Rule>,
        private val defaultRuleBuilder: DefaultRuleBuilder,
        private val positionsRange: IntRange
) {

    fun ruleForSpace(position: Int): String {
        if (notInBoard(position)) throw IllegalArgumentException()

        return findRuleFor(position)?.description() ?: defaultRuleBuilder.build(position)
    }

    private fun findRuleFor(position: Int) = rules.find { it.appliesTo(position) }

    private fun notInBoard(position: Int) = position !in positionsRange

}