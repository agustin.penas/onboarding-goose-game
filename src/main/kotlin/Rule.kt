interface Rule{

    fun appliesTo(position:Int):Boolean

    fun description():String

}

class TheBridgeRule : Rule {
    override fun appliesTo(position: Int): Boolean = position == 6

    override fun description(): String = "The Bridge: Go to space 12"
}

class TheHotelRule : Rule {
    override fun appliesTo(position: Int) = position == 19

    override fun description() = "The Hotel: Stay for (miss) one turn"
}

/*
isBridge(position) -> "The Bridge: Go to space 12"
isHotel(position) -> "The Hotel: Stay for (miss) one turn"
isTheWell(position) -> "The Well: Wait until someone comes to pull you out - they then take your place"
isTheMaze(position) -> "The Maze: Go back to space 39"
isPrison(position) -> "The Prison: Wait until someone comes to release you - they then take your place"
isMoveTwoForward(position) -> "Move two spaces forward"
*/

/*
private fun isBridge(position: Int) = position == 6
private fun isMoveTwoForward(position: Int) = position % 6 == 0
private fun isHotel(position: Int) = position == 19
private fun isTheWell(position: Int) = position == 31
private fun isTheMaze(position: Int) = position == 42
private fun isPrison(position: Int) = position in 50..55
*/
