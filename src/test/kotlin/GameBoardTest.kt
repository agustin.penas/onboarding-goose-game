import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class GameBoardTest {

    private lateinit var gameBoard: GameBoard

    private lateinit var ruleDescription: String

    private lateinit var exceptionCatched: Exception

    @Test
    fun `when space is out of bounds then illegal argument is thrown`() {
        givenBoardWithNoRules(1..2)

        whenFetchingRuleFor(4)

        thenIllegalArgumentExceptionIsThrown()
    }

    @Test
    fun `when fetching rule for a matching position then description matches`() {
        givenBoardWith(1..6,EmptyDefaultRuleBuilder(), TheBridgeRule())

        whenFetchingRuleFor(6)

        thenRuleDescriptionIs("The Bridge: Go to space 12")
    }

    @Test
    fun `when fetching rule for board with multiple rules return the first that matches`() {
        givenBoardWith(1..19,EmptyDefaultRuleBuilder(), TheBridgeRule(), TheHotelRule())

        whenFetchingRuleFor(19)

        thenRuleDescriptionIs("The Hotel: Stay for (miss) one turn")
    }

    @Test
    fun `when position does not match then default rule is returned`() {
        givenBoardWith(1..1, StayInSpaceBuilder(), TheBridgeRule(), TheHotelRule())

        whenFetchingRuleFor(1)

        thenRuleDescriptionIs("Stay in space 1")
    }

    private fun givenBoardWith(range: IntRange, defaultRuleBuilder: DefaultRuleBuilder, vararg rules: Rule) {
        gameBoard = GameBoard(rules.toList(), defaultRuleBuilder, range)
    }

    private fun givenBoardWithNoRules(range:IntRange) {
        givenBoardWith(range, EmptyDefaultRuleBuilder(), EmptyRule())
    }

    private fun whenFetchingRuleFor(position: Int) {
        try {
            ruleDescription = gameBoard.ruleForSpace(position)
        } catch (e: Exception) {
            exceptionCatched = e
        }
    }

    private fun thenIllegalArgumentExceptionIsThrown() {
        assertTrue(exceptionCatched is IllegalArgumentException)
    }

    private fun thenRuleDescriptionIs(expectedRuleDescription: String) {
        assertEquals(expectedRuleDescription, ruleDescription)
    }

}

class EmptyRule : Rule {
    override fun appliesTo(position: Int) = false

    override fun description() = ""
}

class EmptyDefaultRuleBuilder : DefaultRuleBuilder {

    override fun build(position: Int) = ""

}